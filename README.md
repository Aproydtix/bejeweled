# Bejeweled
Made in Unity using C#.

Math project. We were tasked with making Bejeweled.
I added lots of extra features like board size, gem count, match amount variable, and more. All of this can also be changed in real time.
Some additional polish was also added, such as lerped movement and scaling, sound effects, particle effects, and music.

## Build
A build can be found as a .zip file in the Downloads section.

## Video Demo
https://youtu.be/JtygIQkqBxE

## Screenshots
![Screenshot1](Screenshot1.png)
![Screenshot2](Screenshot2.png)
![Screenshot3](Screenshot3.png)
