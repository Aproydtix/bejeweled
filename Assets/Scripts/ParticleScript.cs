﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleScript : MonoBehaviour {

    public float direction; //Direction
    public float speed;     //Speed
    float lifespan = 1f;    //Duration
    Vector3 size;           //Initial size
    float gravity = 0;

	// Use this for initialization
	void Start ()
    {
        transform.position += new Vector3(Mathf.Cos(direction) * speed, Mathf.Sin(direction) * speed, 0f); //Start moving
        size = transform.localScale;    //Save initial size
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position += new Vector3(Mathf.Cos(direction) * speed, Mathf.Sin(direction) * speed, 0f); //Move
        gravity += .1f*Time.deltaTime * (1 + 2 * (Time.timeSinceLevelLoad - MainScript.timeSinceHeld));
        transform.position -= new Vector3(0, gravity, 0);
        transform.localScale = size * (lifespan);   //Reduce size to lifespan
        lifespan -= Time.deltaTime;                 //Reduce lifespan
        if (lifespan <= 0)
            Destroy(gameObject);                    //Destroy
	}
}
