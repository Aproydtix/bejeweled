﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JewelScript : MonoBehaviour {

    public int colour;  //Colour of jewel
    public int i;       //row in matrix
    public int j;       //column in matrix

    void Update()
    {
        name = "Jewel(" + i.ToString() + "," + j.ToString() + ")";  //Set name
    }

    public void DestroySelf()
    {
        if (MainScript.timeSinceHeld != 0)
        {
            int ran = Random.Range(0, 100 / MainScript.prevGridSize);
            //Particles
            for (int i = 0; i < ran; i++)
            {
                GameObject particle = new GameObject();         //Create object
                particle.AddComponent<SpriteRenderer>().sprite = GetComponent<SpriteRenderer>().sprite; //Set sprite
                particle.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, .5f);   //Set new alpha
                particle.transform.localScale = transform.localScale / Random.Range(2.5f, 3.5f);              //Set size
                particle.transform.position = transform.position;                       //Set position
                particle.AddComponent<ParticleScript>();                                //Add script
                particle.GetComponent<ParticleScript>().direction = Random.Range(0f, 360f); //Randomize direction
                particle.GetComponent<ParticleScript>().speed = Random.Range(.01f, .06f) * (1 + 2 * (Time.timeSinceLevelLoad - MainScript.timeSinceHeld));   //Randomize speed
            }
        }
        Destroy(gameObject);
    }
}
