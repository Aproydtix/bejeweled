﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class MainScript : MonoBehaviour
{
    public GameObject prefab;       //Jewel object prefab
    public Sprite[] sprites;        //Jewel sprites

    public static int gridSize = 16;    //Size of board GxG
    public static int prevGridSize;     //Previous size
    static int jewels = 7;              //Variations of Jewels
    public static int prevJewels;       //Previous variations
    static int matchLength = 3;         //Length of matching jewels needed for a chain
    public static bool onlySwitchToMatch = true;  //Whether you can only switch jewels if it creates a chain 
    public ulong score;                   //Accumelated score
    int scoreMultiplier;            //Multiplier from combos

    int[,] board;               //Information on where each jewel is in the grid
    GameObject[,] objects;      //The jewel objects
    Vector3[,] pos;             //The position the jewels should move to
    bool[,] destroy;            //Whether the jewel should be destroyed, true if a pattern is found
    bool[,] inPosition;         //If the jewel object has moved into place
    
    GameObject held;            //The object held by the mouse
    public static float timeSinceHeld;        //A timer used to speed up movement of jewels over time, reset when player interacts

    int input;                  //A number the player can input to change values
    bool inputGrid;             //Whether the player is inputting to gridSize
    bool inputJewels;           //Whether the player is inputting to jewels
    bool inputMatch;            //Whether the player is inputting to matchLength
    KeyCode[] key = new KeyCode[10];    //The number keys 0-9 for input

    float screenShake;          //Amount and duration of screenshake

    // Use this for initialization
    void Start()
    {
        gridSize = Mathf.Clamp(gridSize, 5, 100);   //Clamp gridSize
        jewels = Mathf.Clamp(jewels, 2, 7);         //Clamp jewels
        matchLength = Mathf.Clamp(matchLength, 2, gridSize/2);    //Clamp chain length
        board = new int[100, 100];                  //Initialize to max size
        objects = new GameObject[100, 100];         //Initialize to max size
        pos = new Vector3[100, 100];                //Initialize to max size
        destroy = new bool[100, 100];               //Initialize to max size
        inPosition = new bool[100, 100];            //Initialize to max size
        timeSinceHeld = 0;                          //Check used for particles

        prevGridSize = gridSize;    //Set prevGridSize
        prevJewels = jewels;        //Set prevJewels

        key[0] = KeyCode.Alpha0;    //Set 0
        key[1] = KeyCode.Alpha1;    //Set 1
        key[2] = KeyCode.Alpha2;    //Set 2
        key[3] = KeyCode.Alpha3;    //Set 3
        key[4] = KeyCode.Alpha4;    //Set 4
        key[5] = KeyCode.Alpha5;    //Set 5
        key[6] = KeyCode.Alpha6;    //Set 6
        key[7] = KeyCode.Alpha7;    //Set 7
        key[8] = KeyCode.Alpha8;    //Set 8
        key[9] = KeyCode.Alpha9;    //Set 9

        CreateJewels();             //Spawns jewels above screen
        bool inPlace = false;   //Whether all jewel objects are in place
        do
        {
            UpdateJewels();     //Updates Jewel size and info, moves jewels downwards in board[,]
            CheckPatterns();    //Checks for matching jewels
            MoveJewels();       //Moves jewels towards destination
            DestroyJewels();    //Destroys Jewels that have been matched in CheckPatterns()
            UpdateJewels();     //Updates Jewels again after destruction
            CreateJewels();     //Creates new Jewels if there are empty spaces

            inPlace = true;     //Whether all jewel objects are in place
            for (int i = 0; i < gridSize; i++)
                for (int j = 0; j < gridSize; j++)
                    if (!inPosition[i, j] || objects[i,j] == null || board[i,j] == 0 || destroy[i,j])
                        inPlace = false;  //Set to false if at least one jewel is not in place
        } while (!inPlace);

        score = 0;                                  //Set to 0
        screenShake = 0;                            //Set to 0
        timeSinceHeld = Time.timeSinceLevelLoad;    //Reset
        for (int i = 0; i < gridSize; i++)
            for (int j = 0; j < gridSize; j++)
                if (objects[i, j] != null && objects[i, j] != held) //if object exists and isn't being held
                {
                    float w = Screen.width / 1920f;     //Screen ratio
                    pos[i, j] = Camera.main.ScreenToWorldPoint(new Vector3(w * 420 + (i + .5f) * ((float)(Screen.width - w * 840) / gridSize), 2 * Screen.height - (j + .5f) * ((float)Screen.height / gridSize), 1));
                    objects[i, j].transform.position = pos[i, j];   //Above screen
                }
    }

    // Update is called once per frame
    void Update()
    {
        InputNumber();          //Inputs numbers if inputGrid or inputJewels is true
        gridSize = Mathf.Clamp(gridSize, 5, 100);   //Clamp gridSize from input
        jewels = Mathf.Clamp(jewels, 2, 7);         //Clamp jewels from input
        matchLength = Mathf.Clamp(matchLength, 2, gridSize / 2);    //Clamp chain length
        if (gridSize != prevGridSize)   //If size changed
            ResizeGrid();               //Resize grid
        prevGridSize = gridSize;        //Set prevGrid to gridSize
        if (jewels != prevJewels)       //If jewels changed
            DeleteJewelColours();       //Set prevJewels to jewels

        ScreenShake();      //Performs and reduces screenshake

        UpdateJewels();     //Updates Jewel size and info, moves jewels downwards in board[,]
        CheckPatterns();    //Checks for matching jewels
        MoveJewels();       //Moves jewels towards destination
        bool inPos = true;  //Whether all jewel objects are in place
        for (int i = 0; i < gridSize; i++)
            for (int j = 0; j < gridSize; j++)
                if (!inPosition[i, j])
                    inPos = false;  //Set to false if at least one jewel is not in place
        if (!inPos)         //If not in place
        {
            held = null;    //Player cannot move jewels
            return;         //Return!
        }

        DestroyJewels();    //Destroys Jewels that have been matched in CheckPatterns()
        UpdateJewels();     //Updates Jewels again after destruction

        bool canMove = true;
        for (int i = 0; i < gridSize; i++)
            for (int j = 0; j < gridSize; j++)
                if (board[i, j] == 0)
                    canMove = false;
        if (canMove)
            SwitchJewels();     //Player can move jewels

        CreateJewels();     //Creates new Jewels if there are empty spaces
    }

    void OnGUI()
    {
        float w = Screen.width / 1920f;     //ratio to keep scaling regardless of resolution
        float h = Screen.height / 1080f;    //ratio to keep scaling regardless of resolution
        float bw = 150;     //Button width
        float bh = 60;      //Button height
        float bs = 10;      //Button space
        int bc = 0;         //Button count
        GUI.skin.button.fontSize = (int)(h*40f*(w/h));    //Set fontsize

        //InputGrid Button
        bc++;
        GUI.color = Color.grey;         //Grey
        if (inputGrid)
            GUI.color = Color.white;    //White if active
        //If Button clicked
        GUI.Button(new Rect(w*bs, h*bs*bc + h*bh*(bc-1), w*bw, h*bh),"Size:");
        if (GUI.Button(new Rect(w*bs + w*bw, h*bs*bc + h*bh*(bc-1), w*bw, h*bh), (inputGrid)?((input == 0)?"_":(input).ToString()):gridSize.ToString()))
        {
            inputGrid = !inputGrid;     //On/Off switch
            inputJewels = false;        //Set other input to false
            inputMatch = false;         //Set other input to false
            input = 0;                  //Reset input number
        }

        //InputJewel Button
        bc++;
        GUI.color = Color.grey;         //Grey
        if (inputJewels)
            GUI.color = Color.white;    //White if active
        //If Button clicked
        GUI.Button(new Rect(w*bs, h*bs*bc + h*bh*(bc-1), w*bw, h*bh), "Gems:");
        if (GUI.Button(new Rect(w*bs + w*bw, h*bs*bc + h*bh*(bc-1), w*bw, h*bh), (inputJewels)?((input == 0)?"_":(input).ToString()):jewels.ToString()))
        {
            inputJewels = !inputJewels; //On/Off switch
            inputGrid = false;          //Set other input to false
            inputMatch = false;         //Set other input to false
            input = 0;                  //Reset input number
        }

        //InputMatch Button
        bc++;
        GUI.color = Color.grey;         //Grey
        if (inputMatch)
            GUI.color = Color.white;    //White if active
        //If Button clicked
        GUI.Button(new Rect(w*bs, h*bs*bc + h*bh*(bc-1), w*bw, h*bh),"Match:");
        if (GUI.Button(new Rect(w*bs + w*bw, h*bs*bc + h*bh*(bc-1), w*bw, h*bh), (inputMatch) ? ((input == 0) ? "_" : (input).ToString()) : matchLength.ToString()))
        {
            inputMatch = !inputMatch;   //On/Off switch
            inputGrid = false;          //Set other input to false
            inputJewels = false;        //Set other input to false
            input = 0;                  //Reset input number
        }

        //OnlyMatchSwitch
        bc++;
        GUI.color = Color.grey;         //Grey
        if (onlySwitchToMatch)
            GUI.color = Color.white;    //White if active
        //If Button clicked
        GUI.Button(new Rect(w*bs, h*bs*bc + h*bh*(bc-1), w*bw, h*bh), "Switch:");
        if (GUI.Button(new Rect(w*bs + w*bw, h*bs*bc + h*bh*(bc-1), w*bw, h*bh), (onlySwitchToMatch) ? "On" : "Off"))
            onlySwitchToMatch = !onlySwitchToMatch;
        //Reset Button
        bc++;
        GUI.color = Color.red;         //Red
        if (GUI.Button(new Rect(w*bs + w*bw/2f, h*bs*bc + h*bh*(bc-.5f), w*bw, h*bh), "Reset"))
            Application.LoadLevel(Application.loadedLevel); //Reload current level

        //Score
        GUI.color = Color.cyan;
        GUI.Button(new Rect(Screen.width - w*bs - w*bw, h*bs, w*bw, h*bh), "Score");
        if (score > 0)
        {
            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
            nfi.NumberDecimalDigits = 0;
            GUI.Button(new Rect(Screen.width - w * bs - w * bw * (1 + (int)Mathf.Log10(score)) * .2f, h * bs * 2 + h * bh, w * bw * (1 + (int)Mathf.Log10(score)) * .2f, h * bh), score.ToString("N", nfi));
        }

        //Quit
        GUI.color = Color.red;
        if (GUI.Button(new Rect(Screen.width - w * bs - w * bw, Screen.height - h * bs*7, w * bw, h * bh), "Quit"))
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }

    void CreateJewels()
    {
        for (int i = 0; i < gridSize; i++)
            for (int j = 0; j < gridSize; j++)
            {
                if (board[i, j] == 0)                           //If position is empty
                    board[i, j] = Random.Range(1, jewels + 1);  //Add random jewel

                if (objects[i, j] == null)  //If position isn't empty but has no object
                {
                    float w = Screen.width / 1920f;             //Screen width ratio
                    //Set spawn position above screen
                    pos[i, j] = Camera.main.ScreenToWorldPoint(new Vector3(w * 420 + (i + .5f) * ((float)(Screen.width - w * 840) / gridSize), Screen.height*2 - (j + .5f) * ((float)Screen.height / gridSize), 1));
                    objects[i, j] = Instantiate(prefab, pos[i,j], Quaternion.identity);                 //Create object
                    objects[i, j].GetComponent<JewelScript>().i = i;                                    //Set Row
                    objects[i, j].GetComponent<JewelScript>().i = j;                                    //Set Column
                    objects[i, j].GetComponent<JewelScript>().colour = board[i, j];                     //Set value
                    objects[i, j].GetComponent<SpriteRenderer>().sprite = sprites[board[i, j] - 1];     //Set sprite to jewel value
                    objects[i, j].transform.localScale = new Vector3((17.5f / gridSize) * (Screen.width / Screen.height), (17.5f / gridSize), 1);   //Set size
                }
            }
    }

    void CheckPatterns()
    {
        for (int i = 0; i < gridSize; i++)
            for (int j = 0; j < gridSize; j++)
                if (board[i,j] != 0)        //if position isn't empty
                {
                    //Horizontal
                    if (i + matchLength < gridSize+1)   //if position X spaces to the right isn't off the grid
                        for (int k = 1; k < matchLength; k++)
                        {
                            if (board[i, j] != board[i + k, j])         //Break if not matching
                                break;
                            if (k == matchLength - 1)                   //if matching jewels
                                for (int l = 0; l < matchLength; l++)
                                    destroy[i + l, j] = true;           //set all to be destroyed
                        }
                    //Vertical
                    if (j + matchLength < gridSize+1)   //if position X spaces down isn't off the grid
                        for (int k = 1; k < matchLength; k++)
                        {
                            if (board[i, j] != board[i, j+k])         //Break if not matching
                                break;
                            if (k == matchLength - 1)                   //if matching jewels
                                for (int l = 0; l < matchLength; l++)
                                    destroy[i, j + l] = true;           //set all to be destroyed
                        }
                }  
    }

    void DestroyJewels()
    {
        for (int i = 0; i < gridSize; i++)
            for (int j = 0; j < gridSize; j++)
                if (board[i, j] != 0)       //if position isn't empty
                    if (destroy[i, j])      //if jewel is to be destroyed
                    {
                        board[i, j] = 0;        //Remove data from grid
                        destroy[i, j] = false;  //Position in data should no longer be destroyed
                        if (objects[i, j])
                            objects[i, j].GetComponent<JewelScript>().DestroySelf(); //Destroy jewel object
                        objects[i, j] = null;   //Remove object from matrix
                        scoreMultiplier++;      //Increase multiplier
                        score += (ulong)(scoreMultiplier * 10);  //Increase score
                        screenShake = .25f;     //Set screenshake
                        if (timeSinceHeld != 0)
                        {
                            GetComponent<AudioSource>().Play(); //Play sound
                            //Set pitch to inrease over time since last input with slight randomness
                            GetComponent<AudioSource>().pitch = (1 + (Time.timeSinceLevelLoad - timeSinceHeld) / 5f) * Random.Range(0.95f, 1.05f);
                            //Clamp pitch
                            GetComponent<AudioSource>().pitch = Mathf.Clamp(GetComponent<AudioSource>().pitch, .95f, 2f);
                        }
                    }
    }

    void UpdateJewels()
    {
        for (int i = 0; i < gridSize; i++)
            for (int j = gridSize - 2; j >= 0; j--)         //Checks through jewels from the bottom!
                if (board[i, j] != 0 && j + 1 < gridSize)   //if position isn't empty and position below isn't off grid
                    for (int k = j; k < gridSize; k++)      //Loops from j to move jewel all the way down
                    {
                        if (k + 1 >= gridSize || board[i, k + 1] != 0)
                            break;  //break if bottom-most jewel or there is a jewel below

                        board[i, k + 1] = board[i, k];      //Set jewel below to current jewel
                        board[i, k] = 0;                    //Set current jewel to 0
                        objects[i, k + 1] = objects[i, k];  //Set object below to current object
                        objects[i, k] = null;               //Set current object to null
                    }

        for (int i = 0; i < gridSize; i++)
            for (int j = 0; j < gridSize; j++)
                if (objects[i, j] != null)   //if object exists
                {
                    //Change scale over time to match screen/grid -size
                    objects[i, j].transform.localScale = Vector3.Lerp
                        (
                        objects[i, j].transform.localScale,
                        new Vector3((10f / gridSize) * ((Screen.width * .8f) / Screen.height), (17.5f / gridSize), 1),
                        5f * Time.deltaTime * (1 + 2 * (Time.timeSinceLevelLoad - timeSinceHeld))
                        );
                    objects[i, j].GetComponent<JewelScript>().i = i;                //Set row to jewelscript 
                    objects[i, j].GetComponent<JewelScript>().j = j;                //Set column to jewelscript
                    objects[i, j].GetComponent<JewelScript>().colour = board[i, j]; //Set jewel value to script
                }
    }

    void MoveJewels()
    {
        for (int i = 0; i < gridSize; i++)
            for (int j = 0; j < gridSize; j++)
                if (objects[i, j] != null && objects[i, j] != held) //if object exists and isn't being held
                {
                    float w = Screen.width / 1920f;     //Screen ratio
                    //Set position to move towards
                    pos[i, j] = Camera.main.ScreenToWorldPoint(new Vector3(w*420 + (i + .5f) * ((float)(Screen.width-w*840) / gridSize), Screen.height - (j + .5f) * ((float)Screen.height / gridSize), 1));
                    //Move towards position
                    objects[i, j].transform.position = Vector3.Lerp
                        (
                        objects[i, j].transform.position,   //Current pos
                        pos[i, j],                          //Destination
                        5f * Time.deltaTime * (1 + 2*(Time.timeSinceLevelLoad - timeSinceHeld)) //Speed, increases
                        );
                    if (Vector3.Distance(objects[i, j].transform.position, pos[i,j]) < .1f)
                        inPosition[i, j] = true;    //if object has reached destination
                    else
                        inPosition[i, j] = false;
                }

    }

    void SwitchJewels()
    {
        scoreMultiplier = 0;    //Set multiplier to 0
        Vector3 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);    //Mouse position
        if (Input.GetMouseButtonDown(0))            //Left mouse button
        {
            RaycastHit2D hit = Physics2D.Raycast(mouse, new Vector3(0, 0, 1));  //Ray
            if (hit)    //if mouse is on a jewel
            {
                held = hit.transform.gameObject;    //Object to hold is object hit
                held.GetComponent<BoxCollider2D>().enabled = false; //Disable the collider so next ray doesn't hit this object
            }
        }
        if (held)       //if an object is being held
        {
            timeSinceHeld = Time.timeSinceLevelLoad;        //Reset timer that increases game speed
            held.transform.position = new Vector3(mouse.x, mouse.y, -9.00001f); //Set object position to mouse
            if (Input.GetMouseButtonUp(0))                  //Left mouse button
            {
                int i = held.GetComponent<JewelScript>().i; //Get object's row in matrix
                int j = held.GetComponent<JewelScript>().j; //Get object's column in matrix
                RaycastHit2D hit = Physics2D.Raycast(mouse, new Vector3(0, 0, 1));  //Ray for new object
                if (hit)    //if mouse is on a new jewel
                {
                    int oi = hit.transform.GetComponent<JewelScript>().i;   //Other's row
                    int oj = hit.transform.GetComponent<JewelScript>().j;   //Ohter's column
                    //If other is adjacent to the jewel being held, switch them!
                    if ((i == oi + 1 && j == oj) || (i == oi - 1 && j == oj) || (i == oi && j == oj + 1) || (i == oi && j == oj - 1))
                    {
                        int tb = board[i, j];           //Temp jewel value to held jewel
                        GameObject to = objects[i, j];  //Temp jewel object to held jewel
                        board[i, j] = board[oi, oj];    //Held jewel value is now the other's
                        objects[i, j] = objects[oi, oj];//Held jewel object is now the other's
                        board[oi, oj] = tb;             //Set the other's value to temp
                        objects[oi, oj] = to;           //Set the other's object to temp
                    }
                    
                    if (onlySwitchToMatch)
                    {
                        CheckPatterns();//Marks jewels to be deleted
                        bool change = false; //Set to false again to check whether any jewels will be destroyed
                        for (int k = 0; k < gridSize; k++)
                            for (int l = 0; l < gridSize; l++)
                                if (destroy[k, l])      //Jewel will be destroyed
                                    change = true;      //Change has occured
                        if (!change)
                            if ((i == oi + 1 && j == oj) || (i == oi - 1 && j == oj) || (i == oi && j == oj + 1) || (i == oi && j == oj - 1))
                            {
                                int tb = board[i, j];           //Temp jewel value to held jewel
                                GameObject to = objects[i, j];  //Temp jewel object to held jewel
                                board[i, j] = board[oi, oj];    //Held jewel value is now the other's
                                objects[i, j] = objects[oi, oj];//Held jewel object is now the other's
                                board[oi, oj] = tb;             //Set the other's value to temp
                                objects[oi, oj] = to;           //Set the other's object to temp
                            }
                    }
                }
                held.GetComponent<BoxCollider2D>().enabled = true;  //Re-enable collider on the held object
                held = null;    //No longer hold object
            }
        }
    }

    void ResizeGrid()
    {
        if (prevGridSize > gridSize)    //if grid has become smaller
        {
            for (int i = 0; i < prevGridSize; i++)
                for (int j = 0; j < prevGridSize; j++)
                    if (i >= gridSize || j >= gridSize) //if jewel is outside new size
                    {
                        board[i, j] = 0;            //Set jewel value to 0
                        if (objects[i, j] != null)  //if object exists
                            Destroy(objects[i, j]); //Destroy it
                        destroy[i, j] = false;      //Reset whether object should be destroyed
                        inPosition[i, j] = false;   //Reset whether object is in position
                    }
        }
        else    //if grid has become bigger
        {
            UpdateJewels(); //Update values for board
            CreateJewels(); //Create jewels for empty spaces
        }
        timeSinceHeld = Time.timeSinceLevelLoad;    //Reset speed increase
    }

    void DeleteJewelColours()
    {
        for (int i = 0; i < gridSize; i++)
            for (int j = 0; j < gridSize; j++)
                if (board[i,j] >= jewels+1)     //if jewel exists of colour that shouldn't
                {
                    board[i, j] = 0;            //Set jewel value to 0
                    if (objects[i, j] != null)  //if object exists
                        Destroy(objects[i, j]); //Destroy it
                    destroy[i, j] = false;      //Reset whether object should be destroyed
                    inPosition[i, j] = false;   //Reset whether object is in position
                }
        prevJewels = jewels;    //Set prev value to current
        UpdateJewels();         //Update values for board
        CreateJewels();         //Create new jewels to fill in for old ones
        timeSinceHeld = Time.timeSinceLevelLoad;    //Reset speed increase
    }

    void InputNumber()
    {
        if (inputGrid || inputJewels || inputMatch)       //if player is inputting
        {
            for (int i = 0; i < 10; i++)    //for numbers 0-9
            {
                if (Input.GetKeyDown(key[i])) //Number pressed
                {
                    input *= 10;            //Move number up a digit
                    input += i;             //Add new number at the end
                }
            }
            if (Input.GetKeyDown(KeyCode.Backspace))    //Delete last number
                input /= 10;                            //Removes last digit
            input = Mathf.Clamp(input, 0, 100);         //Clamp input

            if (Input.GetKeyDown(KeyCode.Return))   //Enter
            {
                if (inputGrid)
                    gridSize = input;   //Set gridSize to input
                if (inputJewels)
                    jewels = input;     //Set jewels to input
                if (inputMatch)
                    matchLength = input;//Set matchLength to input
                inputGrid = false;      //Deactivate input
                inputJewels = false;    //Deactivate input
                inputMatch = false;     //Deactivate input
                timeSinceHeld = Time.timeSinceLevelLoad;
            }
            if (Input.GetKeyDown(KeyCode.Escape))   //Escape
            {
                inputGrid = false;      //Deactivate input
                inputJewels = false;    //Deactivate input
                inputMatch = false;     //Deactivate input
            }
        }
    }

    void ScreenShake()
    {
        //Set camera position to random value based on screenshake amount
        Camera.main.transform.position = new Vector3(Random.Range(0f, screenShake), Random.Range(0f, screenShake), -10);
        if (screenShake > 0)
            screenShake -= Time.deltaTime;  //Reduce screenshake over time
        else
            screenShake = 0;
    }
}
